package com.hasdev.appscheduler.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hasdev.appscheduler.R
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity
import kotlinx.android.synthetic.main.history_item_layout.view.*


class HistoryAdapter(private val context: Context): RecyclerView.Adapter<HistoryAdapter.ViewHolder>()  {

    var historyList : List<ScheduleHistoryEntity>? = null

    fun setData(historyList : List<ScheduleHistoryEntity>?){
        this.historyList = historyList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryAdapter.ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.history_item_layout,parent,false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: HistoryAdapter.ViewHolder, position: Int) {
        holder.itemView.tvHistoryAppName.text = "${position+1} . ${historyList?.get(position)?.appName}"
        if(historyList?.get(position)?.scheduleCompleted == true){
            holder.itemView.tvStatus.text = context.getString(R.string.completed_str)
        }
        
    }

    override fun getItemCount(): Int {
        return historyList?.size?:0
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    }
}