package com.hasdev.appscheduler.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hasdev.appscheduler.R
import com.hasdev.appscheduler.database.converters.DataConverter
import com.hasdev.appscheduler.database.entities.AppInfoEntity
import kotlinx.android.synthetic.main.home_item_layout.view.*

class HomeAdapter(private val context: Context, private val clickListener: ItemClickListener): RecyclerView.Adapter<HomeAdapter.ViewHolder>()  {

    var homeItemList : List<AppInfoEntity>? = null

    interface ItemClickListener{
        fun onClicked(title: String, pName: String, offClicked: Boolean, changeClicked: Boolean)
    }

    fun setData(homeItemList: List<AppInfoEntity>){
        this.homeItemList = homeItemList
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.home_item_layout,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.tvAppName.text = homeItemList?.get(position)?.appName
        holder.itemView.ivIcon.setImageBitmap(DataConverter().toBitmap(homeItemList?.get(position)?.icon))
        if(homeItemList?.get(position)?.scheduleTime != 0L){
            holder.itemView.ivChangeTime.visibility = View.VISIBLE
            holder.itemView.ivOn.visibility = View.GONE
            holder.itemView.ivOff.visibility = View.VISIBLE
        }else
        {
            holder.itemView.ivOn.visibility = View.VISIBLE
            holder.itemView.ivOff.visibility = View.GONE
            holder.itemView.ivChangeTime.visibility = View.GONE
        }
        holder.itemView.ivOn.setOnClickListener {
            clickListener.onClicked(homeItemList?.get(position)?.appName?:"",homeItemList?.get(position)?.pName?:"",
                offClicked = false,
                changeClicked = false
            )
        }
        holder.itemView.ivOff.setOnClickListener {
            clickListener.onClicked(homeItemList?.get(position)?.appName?:"",homeItemList?.get(position)?.pName?:"",
                offClicked = true,
                changeClicked = false
            )
        }
        holder.itemView.ivChangeTime.setOnClickListener {
            clickListener.onClicked(homeItemList?.get(position)?.appName?:"",homeItemList?.get(position)?.pName?:"",
                offClicked = false,
                changeClicked = true
            )
        }
    }

    override fun getItemCount(): Int {
        return homeItemList?.size?:0
    }
}