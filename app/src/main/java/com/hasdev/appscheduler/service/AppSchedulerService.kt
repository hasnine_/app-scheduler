package com.hasdev.appscheduler.service

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.lifecycle.lifecycleScope
import com.hasdev.appscheduler.BaseService
import com.hasdev.appscheduler.MyApp
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity
import com.hasdev.appscheduler.database.AppSchedulerManager
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.Executors

class AppSchedulerService: BaseService() {

    companion object{
        const val EXTRA_APP_NAME = "extra.app.name"
        const val EXTRA_PACKAGE_NAME = "extra.app.package"

        const val EXTRA_FROM_WORKER = "extra.from.worker"

        const val FROM_WORKER = "from.worker"

        const val BROADCAST_ACTION = "com.appscheduler.service"
    }

    private val appSchedulerManager by lazy {
        AppSchedulerManager(db)
    }

    var intent: Intent? = null

    private val ioCoroutineScope = lifecycleScope
    private val IO_DISPATCHER = Executors.newFixedThreadPool(8).asCoroutineDispatcher()

    override fun onCreate() {
        super.onCreate()
        intent = Intent(BROADCAST_ACTION)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
         super.onStartCommand(intent, flags, startId)

        if(intent != null){
            when{
                intent.getStringExtra(EXTRA_FROM_WORKER) == FROM_WORKER ->{
                    ioCoroutineScope.launch(IO_DISPATCHER){
                        dpOperation(intent.getStringExtra(EXTRA_APP_NAME),intent.getStringExtra(
                            EXTRA_PACKAGE_NAME
                        ),true)
                    }
                    startNewActivity(this,intent.getStringExtra(EXTRA_PACKAGE_NAME))
                }

            }
        }

        return START_STICKY
    }

    private suspend fun dpOperation(appName: String?, pName: String?, status: Boolean){
        (application as MyApp).openDb()
        val currentDate = Calendar.getInstance()
        val historyEntity = ScheduleHistoryEntity(0,appName?:"",pName?:"",currentDate.time.toString(),status)
        appSchedulerManager.insertAppHistory(historyEntity)
        pName?.let { appSchedulerManager.updateAppInfoSchedule(it,0) }
        (application as MyApp).closeDb()
        sendBroadcast(intent)
    }

    private fun startNewActivity(context: Context, packageName: String?) {
        if (packageName?.isNotEmpty() == true) {
            var intent = context.packageManager.getLaunchIntentForPackage(packageName ?: "")
            if (intent == null) {
                intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("market://details?id=$packageName")
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        }
    }

}