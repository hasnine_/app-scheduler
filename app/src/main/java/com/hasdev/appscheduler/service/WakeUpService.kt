package com.hasdev.appscheduler.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.core.app.NotificationCompat
import com.hasdev.appscheduler.R

class WakeUpService: Service() {

    companion object {
        private const val TAG = "WakeupServiceLog"
        private const val CHANNEL_ID = "WakeupService"
        private const val CHANNEL_NAME = "WakeupServiceChannel"
        private const val NOTIFICATION_ID = 1
    }

    private var builder: NotificationCompat.Builder?= null
    private var notificationManager: NotificationManager?= null
    private var currentServiceType: ServiceStarter.ServiceType?= null
    private val handler = Handler(Looper.getMainLooper())
    private val stopRunnable = Runnable {
        removeNotification()
        stopSelf()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        handler.removeCallbacks(stopRunnable)
        val workerIntent = intent?.getParcelableExtra<Intent>(ServiceStarter.KEY_WORKER_INTENT)
        if (workerIntent != null) {
            val serviceType: ServiceStarter.ServiceType = workerIntent.getSerializableExtra(
                ServiceStarter.KEY_WORKER_TYPE
            ) as ServiceStarter.ServiceType
            if (currentServiceType != serviceType) {
                currentServiceType = serviceType
                updateNotificationContent(serviceType)
            }
            startService(workerIntent)
        }
        handler.postDelayed(stopRunnable, 2000L)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onCreate() {
        super.onCreate()
        showNotification()
    }

    private fun updateNotificationContent(serviceType: ServiceStarter.ServiceType) {
        val contentText = when (serviceType) {
            ServiceStarter.ServiceType.AppSchedulerService -> {
                getString(R.string.service_start)
            }

        }
        builder?.setContentText(contentText)
        if (notificationManager == null) {
            notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        }
        notificationManager?.notify(NOTIFICATION_ID, builder?.build())
    }

    private fun showNotification() {
        builder = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(getString(R.string.app_name))
            .setContentText("")
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = CHANNEL_NAME
            val descriptionText = ""
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager?.createNotificationChannel(channel)
        }
        startForeground(NOTIFICATION_ID, builder?.build())
    }


    override fun onDestroy() {
        super.onDestroy()
    }

    private fun removeNotification() {
        if (notificationManager == null) {
            notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        }
        notificationManager?.cancel(NOTIFICATION_ID)
    }
}