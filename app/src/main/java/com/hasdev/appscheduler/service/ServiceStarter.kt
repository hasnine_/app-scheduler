package com.hasdev.appscheduler.service

import android.content.Context
import android.content.Intent
import android.os.Build
import java.lang.IllegalStateException

class ServiceStarter {

    enum class ServiceType{
        AppSchedulerService
    }

    companion object{
        const val KEY_WORKER_INTENT: String = "workerIntent"
        const val KEY_WORKER_TYPE: String = "workerType"

        fun start(context: Context, intent: Intent, serviceType: ServiceType) {
            intent.putExtra(KEY_WORKER_TYPE, serviceType)
            try {
                context.startService(intent)
            } catch (e: IllegalStateException) {
                startWakeupService(context, intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        private fun startWakeupService(context: Context, workerIntent: Intent) {
            val intent = Intent(context, WakeUpService::class.java)
            intent.putExtra(KEY_WORKER_INTENT, workerIntent)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }
    }
}