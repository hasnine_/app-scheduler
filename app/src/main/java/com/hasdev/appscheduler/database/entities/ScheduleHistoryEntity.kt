package com.hasdev.appscheduler.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "app_scheduler_history")
data class ScheduleHistoryEntity(
    @PrimaryKey(autoGenerate = true) var id: Long,
    @ColumnInfo(name = "app_name") var appName: String = "",
    @ColumnInfo(name = "p_name") var pName: String = "",
    @ColumnInfo(name = "schedule_time") var scheduleTime: String = "",
    @ColumnInfo(name = "schedule_completed") var scheduleCompleted: Boolean = false
){
}