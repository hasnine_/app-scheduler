package com.hasdev.appscheduler.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "app_info", indices = [Index(value = ["p_name"],unique = true)])
data class AppInfoEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Long,
    @ColumnInfo(name = "app_name") var appName: String = "",
    @ColumnInfo(name = "p_name") var pName: String = "",
    @ColumnInfo(name = "version_name") var versionName: String = "",
    @ColumnInfo(name = "version_code") var versionCode: Int = 0,
    @ColumnInfo(name = "icon") var icon: String = "",
    @ColumnInfo(name = "schedule_time") var scheduleTime: Long = 0,
    @ColumnInfo(name = "schedule_completed") var scheduleCompleted: Boolean = false
){
    constructor(appName: String,pName: String,versionName: String,versionCode: Int,icon: String):
            this(0,appName,pName,versionName,versionCode,icon,0,false)
}