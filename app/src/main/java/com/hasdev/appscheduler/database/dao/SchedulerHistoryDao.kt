package com.hasdev.appscheduler.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity

@Dao
interface SchedulerHistoryDao {

    @Insert
    suspend fun insertAppHistory(history: ScheduleHistoryEntity)

    @Query("select * from app_scheduler_history ORDER BY app_name")
    fun getHistoryLiveData() : LiveData<List<ScheduleHistoryEntity>>

}