package com.hasdev.appscheduler.database.core

import android.content.Context
import androidx.room.*
import com.hasdev.appscheduler.database.converters.DataConverter
import com.hasdev.appscheduler.database.dao.AppInfoDao
import com.hasdev.appscheduler.database.dao.SchedulerHistoryDao
import com.hasdev.appscheduler.database.entities.AppInfoEntity
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity

@Database(
    entities = [AppInfoEntity::class,ScheduleHistoryEntity::class],
    version = 1
)

@TypeConverters(DataConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun appInfoDao(): AppInfoDao
    abstract fun schedulerHistoryDao(): SchedulerHistoryDao
    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            AppDatabase::class.java, "app-scheduler.db")
            .build()
    }
}