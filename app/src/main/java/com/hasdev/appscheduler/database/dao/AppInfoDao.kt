package com.hasdev.appscheduler.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hasdev.appscheduler.database.entities.AppInfoEntity

@Dao
interface AppInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAppInfo(appInfoEntity: AppInfoEntity)

    @Query("UPDATE app_info SET schedule_time = :scheduleTime WHERE p_name = :pName")
    suspend fun updateAppInfoSchedule(pName: String, scheduleTime: Long)

    @Query("UPDATE app_info SET schedule_completed = :status WHERE p_name = :pName")
    suspend fun updateAppInfoStatus(status: Boolean, pName: String)

    @Query("select * from app_info ORDER BY app_name")
    suspend fun getAllAppInfo() : List<AppInfoEntity>

    @Query("select * from app_info ORDER BY app_name")
    fun getAllInfoLiveData() : LiveData<List<AppInfoEntity>>

}