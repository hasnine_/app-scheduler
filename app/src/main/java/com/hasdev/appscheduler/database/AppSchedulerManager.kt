package com.hasdev.appscheduler.database

import androidx.lifecycle.LiveData
import com.hasdev.appscheduler.database.core.AppDatabase
import com.hasdev.appscheduler.database.entities.AppInfoEntity
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity

class AppSchedulerManager(private val db : AppDatabase) {

    suspend fun insertAllData(appInfoEntity: AppInfoEntity){
        db.appInfoDao().insertAppInfo(appInfoEntity)
    }

    suspend fun getAllAppInfoData() : List<AppInfoEntity>{
        return db.appInfoDao().getAllAppInfo()
    }

    fun getAllAppInfoLiveData() : LiveData<List<AppInfoEntity>>{
        return db.appInfoDao().getAllInfoLiveData()
    }

    suspend fun updateAppInfoSchedule(pName: String, scheduleTime: Long){
        return db.appInfoDao().updateAppInfoSchedule(pName,scheduleTime)
    }

    suspend fun updateAppInfoStatus(status: Boolean, pName: String){
        return db.appInfoDao().updateAppInfoStatus(status,pName)
    }

    suspend fun insertAppHistory(history: ScheduleHistoryEntity){
        return db.schedulerHistoryDao().insertAppHistory(history)
    }

    fun getAllScheduleHistoryLiveData() : LiveData<List<ScheduleHistoryEntity>>{
        return db.schedulerHistoryDao().getHistoryLiveData()
    }

}