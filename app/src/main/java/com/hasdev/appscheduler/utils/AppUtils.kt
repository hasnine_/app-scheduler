package com.hasdev.appscheduler.utils

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import com.hasdev.appscheduler.R

@SuppressLint("ResourceAsColor")
fun showErrorSnackBar(view: View, message: String){
    val snack = Snackbar.make(view, message,
        Snackbar.LENGTH_SHORT).setAction("Action", null)
    snack.setActionTextColor(Color.BLUE)
    val snackView = snack.view
    snackView.setBackgroundColor(R.color.color_gray_01)
    val textView =
        snackView.findViewById(R.id.snackbar_text) as TextView
    textView.setTextColor(Color.WHITE)
    textView.textSize = 12f
    snack.show()
}