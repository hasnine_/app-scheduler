package com.hasdev.appscheduler

import android.content.Intent
import androidx.lifecycle.LifecycleService
import com.hasdev.appscheduler.database.core.AppDatabase

abstract class BaseService: LifecycleService() {

    protected val myApp : MyApp by lazy {
        (application as MyApp)
    }
    protected val db : AppDatabase by lazy {
        myApp.db
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }
}