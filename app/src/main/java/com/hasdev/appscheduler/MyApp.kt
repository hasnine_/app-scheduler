package com.hasdev.appscheduler

import android.app.Application
import androidx.room.Room
import com.hasdev.appscheduler.database.core.AppDatabase

class MyApp: Application() {
    lateinit var db : AppDatabase

    private fun initDB(){
        db = Room.databaseBuilder(this,
        AppDatabase::class.java,"app-scheduler.db").build()
    }

    override fun onCreate() {
        super.onCreate()
        initDB()
    }

    fun openDb(){
        initDB()
    }

    fun closeDb() {
        db.close()
    }


}