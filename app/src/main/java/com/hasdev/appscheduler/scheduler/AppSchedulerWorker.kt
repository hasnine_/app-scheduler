package com.hasdev.appscheduler.scheduler


import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.hasdev.appscheduler.service.AppSchedulerService
import com.hasdev.appscheduler.service.AppSchedulerService.Companion.FROM_WORKER
import com.hasdev.appscheduler.service.ServiceStarter


class AppSchedulerWorker(private val context: Context, workerParams: WorkerParameters) : Worker(context,workerParams) {

    override fun doWork(): Result {
        val workerTag = inputData.getString("worker_tag")
        val appName = inputData.getString("app_name")
        Log.e("CHECK_WORK","Here is the output: $appName")
        startService(appName,workerTag)
        return Result.success()
    }

    private fun startService(appName: String?, pName: String?){
        val intent = Intent(context, AppSchedulerService::class.java)
        intent.putExtra(
            AppSchedulerService.EXTRA_APP_NAME,
            appName
        )
        intent.putExtra(
            AppSchedulerService.EXTRA_PACKAGE_NAME,
            pName
        )
        intent.putExtra(AppSchedulerService.EXTRA_FROM_WORKER, FROM_WORKER)
        ServiceStarter.start(context, intent, ServiceStarter.ServiceType.AppSchedulerService)
    }
}