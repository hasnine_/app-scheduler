package com.hasdev.appscheduler.scheduler

import android.content.Context
import androidx.work.*
import java.util.concurrent.TimeUnit

class AppSchedulerInitializer(private val context: Context) {

    companion object{
        private const val WORKER_TAG = "worker_tag"
        private const val APP_NAME = "app_name"
    }

    private val workManager = WorkManager.getInstance(context)

    fun initAppScheduler(delay: Long, workerTag: String, appName: String) {
        workManager.cancelAllWorkByTag(workerTag)

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.NOT_REQUIRED)
            .build()


        val inputData = Data.Builder()
        inputData.putString(WORKER_TAG, workerTag)
        inputData.putString(APP_NAME, appName)

        val workRequest = OneTimeWorkRequestBuilder<AppSchedulerWorker>()
            .setConstraints(constraints)
            .setInputData(inputData.build())
            .setInitialDelay(delay, TimeUnit.MILLISECONDS)
            .addTag(workerTag)
            .setBackoffCriteria(
                BackoffPolicy.LINEAR,
                OneTimeWorkRequest.MIN_BACKOFF_MILLIS,
                TimeUnit.MILLISECONDS
            )
            .build()
        WorkManager.getInstance(context).enqueueUniqueWork(
            workerTag,
            ExistingWorkPolicy.REPLACE, workRequest
        )

    }

    fun workCancel(workerTag: String){
        workManager.cancelAllWorkByTag(workerTag)
    }
}