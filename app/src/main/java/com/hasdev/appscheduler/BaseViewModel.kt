package com.hasdev.appscheduler

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.hasdev.appscheduler.database.core.AppDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

open class BaseViewModel (application: Application): AndroidViewModel(application) {
    private val viewModelJob = SupervisorJob()
    protected val uiCoroutineScope = CoroutineScope(Dispatchers.Main + viewModelJob)
    protected val ioCoroutineScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    private val myApp : MyApp by lazy {
        (application as MyApp)
    }

    protected val db : AppDatabase by lazy {
        myApp.db
    }


}