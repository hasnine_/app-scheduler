package com.hasdev.appscheduler.ui.history

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.hasdev.appscheduler.BaseActivity
import com.hasdev.appscheduler.R
import com.hasdev.appscheduler.adapter.HistoryAdapter
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.loader_layout.*

class HistoryActivity : BaseActivity() {


    private var adapter: HistoryAdapter? = null

    private val viewModel by lazy {
        ViewModelProvider(this).get(HistoryViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        initView()
    }

    private fun initView(){
        setupToolbar()
        showProgressBar(rlLoader)
        adapter = HistoryAdapter(this)
        rvHistory.hasFixedSize()
        rvHistory.adapter = adapter
        rvHistory.layoutManager = LinearLayoutManager(this)
        viewModel.getHistoryLiveData().observe(this,appHistoryObserver)
    }

    @SuppressLint("NotifyDataSetChanged")
    private val appHistoryObserver = Observer<List<ScheduleHistoryEntity>>{
        hideProgressBar(rlLoader)
        if (it.isNotEmpty()){
            tvEmptyList.visibility = View.GONE
            adapter?.setData(it)
            adapter?.notifyDataSetChanged()
        }
        else
        {
            tvEmptyList.visibility = View.VISIBLE
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun setupToolbar(){
        setSupportActionBar(historyToolbar)
        historyToolbar.navigationIcon = getDrawable(R.drawable.arrow_back_white_24)
        historyToolbar.setNavigationOnClickListener {
            onBackPressed()
        }

    }
}