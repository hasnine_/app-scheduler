package com.hasdev.appscheduler.ui.splash

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.hasdev.appscheduler.database.AppSchedulerManager
import com.hasdev.appscheduler.BaseViewModel
import com.hasdev.appscheduler.database.converters.DataConverter
import com.hasdev.appscheduler.database.entities.AppInfoEntity
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo


class SplashViewModel(val app: Application): BaseViewModel(app)  {

    private val appSchedulerManager by lazy {
        AppSchedulerManager(db)
    }

    @SuppressLint("QueryPermissionsNeeded")
    suspend fun getInstalledApps( context: Context) {
        val packs = context.packageManager.getInstalledPackages(0)
        packs.filter { it.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0}
        for (i in packs.indices) {
            val p = packs[i]
            if (p.versionName == null) {
                continue
            }
            val newInfo = AppInfoEntity(p.applicationInfo.loadLabel(context.packageManager).toString(),p.packageName,p.versionName,
                p.versionCode,DataConverter().fromDrawable(p.applicationInfo.loadIcon(context.packageManager)))
            if(newInfo.appName.isNotEmpty() && !isSystemPackage(p)) {
                appSchedulerManager.insertAllData(newInfo)
            }
        }
    }

    private fun isSystemPackage(pkgInfo: PackageInfo): Boolean {
        return pkgInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0
    }


}