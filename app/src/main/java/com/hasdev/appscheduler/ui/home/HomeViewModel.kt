package com.hasdev.appscheduler.ui.home

import android.app.Application
import androidx.lifecycle.LiveData
import com.hasdev.appscheduler.database.AppSchedulerManager
import com.hasdev.appscheduler.BaseViewModel
import com.hasdev.appscheduler.MyApp
import com.hasdev.appscheduler.database.entities.AppInfoEntity
import kotlinx.coroutines.launch

class HomeViewModel(val app: Application): BaseViewModel(app) {

    private val appSchedulerManager by lazy {
        AppSchedulerManager(db)
    }

    fun getAppInfoLiveData(): LiveData<List<AppInfoEntity>>{
        return appSchedulerManager.getAllAppInfoLiveData()
    }

    fun updateInfo(pName: String,scheduleTime: Long){
        ioCoroutineScope.launch {
            (app as MyApp).openDb()
            appSchedulerManager.updateAppInfoSchedule(pName?:"",scheduleTime)
        }
    }

    suspend fun getAppInfoData(): List<AppInfoEntity>{
        return db.appInfoDao().getAllAppInfo()
    }

//    private suspend fun updateAppInfoUpdate(pName: String?){
//        (app as MyApp).openDb()
//        appSchedulerManager.updateAppInfoSchedule(pName?:"",0)
//    }
}