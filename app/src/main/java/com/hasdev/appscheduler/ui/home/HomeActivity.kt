package com.hasdev.appscheduler.ui.home

import android.annotation.SuppressLint
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.hasdev.appscheduler.BaseActivity
import com.hasdev.appscheduler.adapter.HomeAdapter
import com.hasdev.appscheduler.database.entities.AppInfoEntity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.loader_layout.*
import java.util.*
import android.text.format.DateFormat
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import com.hasdev.appscheduler.MyApp
import com.hasdev.appscheduler.R
import com.hasdev.appscheduler.scheduler.AppSchedulerInitializer
import com.hasdev.appscheduler.ui.history.HistoryActivity
import com.hasdev.appscheduler.service.AppSchedulerService
import com.hasdev.appscheduler.utils.showErrorSnackBar
import kotlinx.coroutines.launch


class HomeActivity : BaseActivity(), HomeAdapter.ItemClickListener, DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener  {

    private var adapter: HomeAdapter? = null
    private val initializer by lazy {   AppSchedulerInitializer((application as MyApp).applicationContext)}

    var day = 0
    var month: Int = 0
    var year: Int = 0
    var hour: Int = 0
    var minute: Int = 0
    var myDay = 0
    var myMonth: Int = 0
    var myYear: Int = 0
    var myHour: Int = 0
    var myMinute: Int = 0

    var pName = ""
    var appName = ""
    private val mCalendar: Calendar = Calendar.getInstance()

    private val viewModel by lazy {
        ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        showProgressBar(rlLoader)
        initView()
    }

    private fun initView(){
        adapter = HomeAdapter(this,this)
        rvApp.hasFixedSize()
        rvApp.adapter = adapter
        rvApp.layoutManager = LinearLayoutManager(this)
        viewModel.getAppInfoLiveData().observe(this,appInfoObserver)

        ivHistory.setOnClickListener {
            startActivity(Intent(this,HistoryActivity::class.java))
        }


    }

    @SuppressLint("NotifyDataSetChanged")
    private val appInfoObserver = Observer<List<AppInfoEntity>>{
        hideProgressBar(rlLoader)
        if (it.isNotEmpty()){
            adapter?.setData(it)
            adapter?.notifyDataSetChanged()
        }
    }


    override fun onClicked(
        title: String,
        pName: String,
        offClicked: Boolean,
        changeClicked: Boolean
    ) {
        this.pName = pName
        this.appName = title
        if(offClicked){
            Toast.makeText(this@HomeActivity, "$appName schedule time stopped",Toast.LENGTH_SHORT).show()
            ioCoroutineScope.launch {
                initializer.workCancel(pName)
                viewModel.updateInfo(pName,0)
            }
        }else {
            setDate()
        }
    }


    private fun setDate() {
        val calendar: Calendar = Calendar.getInstance()
        day = calendar.get(Calendar.DAY_OF_MONTH)
        month = calendar.get(Calendar.MONTH)
        year = calendar.get(Calendar.YEAR)
        calendar.timeZone = TimeZone.getTimeZone("Asia/Dhaka")
        val datePickerDialog =
            DatePickerDialog(this@HomeActivity, this@HomeActivity, year, month,day)
        datePickerDialog.show()
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        myDay = day
        myYear = year
        myMonth = month
        val calendar: Calendar = Calendar.getInstance()
        hour = calendar.get(Calendar.HOUR)
        minute = calendar.get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(this@HomeActivity, this@HomeActivity, hour, minute,
            DateFormat.is24HourFormat(this))
        timePickerDialog.show()
    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        myHour = p1
        myMinute = p2
        mCalendar.timeZone = TimeZone.getTimeZone("Asia/Dhaka")
        mCalendar.set(myYear,myMonth,myDay,myHour,myMinute)
        val currentDate = Calendar.getInstance()
        if(mCalendar.timeInMillis>currentDate.timeInMillis) {
            val timeDiff = mCalendar.timeInMillis - currentDate.timeInMillis
            initializer.initAppScheduler(timeDiff,pName, appName)
            viewModel.updateInfo(pName,timeDiff)
            Toast.makeText(this, "Schedule start time set for $appName",Toast.LENGTH_SHORT).show()
        }
        else
        {
            showErrorSnackBar(clHome, getString(R.string.invalid_time))
        }

    }

    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver,  IntentFilter(
                AppSchedulerService.BROADCAST_ACTION)
        )
    }

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            refreshHome()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun refreshHome() {
        if(adapter != null){
            uiCoroutineScope.launch {
                adapter?.setData(viewModel.getAppInfoData())
                adapter?.notifyDataSetChanged()
            }
        }
    }

}