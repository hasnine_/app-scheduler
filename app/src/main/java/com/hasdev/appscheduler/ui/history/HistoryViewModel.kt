package com.hasdev.appscheduler.ui.history

import android.app.Application
import androidx.lifecycle.LiveData
import com.hasdev.appscheduler.BaseViewModel
import com.hasdev.appscheduler.database.entities.ScheduleHistoryEntity
import com.hasdev.appscheduler.database.AppSchedulerManager

class HistoryViewModel(val app: Application): BaseViewModel(app){

    private val appSchedulerManager by lazy {
        AppSchedulerManager(db)
    }

    fun getHistoryLiveData(): LiveData<List<ScheduleHistoryEntity>> {
        return appSchedulerManager.getAllScheduleHistoryLiveData()
    }
}